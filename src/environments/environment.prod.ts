import {environment as local} from './environment.local';

export const environment = Object.assign(local, {production: true});
