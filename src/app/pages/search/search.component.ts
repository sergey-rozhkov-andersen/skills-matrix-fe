import {Component} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {TeamService} from '../../core/services/team.service';
import {UserService} from '../../core/services/user.service';
import {IUser, IUserDetails} from '../../models/userdetails';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
    providers: [TeamService],
})
export class SearchComponent {
    token: string = localStorage.getItem('access-token');
    result: IUser[] = [];
    searchString = '';

    constructor(
        private searchService: TeamService,
        private httpService: UserService,
        private toastrService: ToastrService,
    ) {
    }

    search() {
        localStorage.setItem('access-token', this.token);

        this.searchService
            .getSearch(this.searchString)
            .subscribe(
                (res: IUser[]) => {
                    this.result = res;
                },
            );
    }

    loadUserDetails(user) {
        this.searchService
            .getUser(user.id)
            .subscribe(
                (res) => {
                    console.log('getUser', res);
                },
                (err) => {
                    console.log('getUser', err);
                },
            );
    }

    addUser(user) {

        this.searchService
            .getUser(user.id)
            .subscribe(
                (res: IUserDetails) => {
                    this.httpService.create(res).then(() => {
                        this.toastrService.success('Пользователь успешно добавлен!');
                    });
                },
                () => {
                    this.toastrService.error('Ошибка получения пользователя!');
                },
            );
    }
}
