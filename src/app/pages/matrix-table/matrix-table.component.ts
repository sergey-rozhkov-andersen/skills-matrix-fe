import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {MatrixService} from '../../core/services/matrix.service';
import {Matrix} from '../../models/matrix';
import {MatrixDialogComponent} from '../../shared/matrix-dialog/matrix-dialog.component';

@Component({
    selector: 'app-matrix-table',
    templateUrl: './matrix-table.component.html',
    styleUrls: ['./matrix-table.component.scss'],
})
export class MatrixTableComponent implements OnInit {

    matrixData: Matrix[];
    displayedColumns: string[] = ['id', 'userId', 'interviewerId', 'topics', 'date', 'results', 'edit'];

    constructor(
        public dialog: MatDialog,
        private matrixService: MatrixService,
        private toastrService: ToastrService,
    ) {
    }

    ngOnInit(): void {
        this.loadData();
    }

    loadData() {
        this.matrixService.getAll().subscribe((res: Matrix[]) => {
            this.matrixData = res;
        }, (err) => {
            console.log(err);
        });
    }

    openAddDialog(): void {
        const dialogRef = this.dialog.open(MatrixDialogComponent, {
            data: {},
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                result.id = Date.now().toString(16);
                this.matrixService.create(result).then(() => {
                    this.toastrService.success('Запись добавлена!');
                });
            }
        });
    }

    openEditDialog(item): void {
        const dialogRef = this.dialog.open(MatrixDialogComponent, {
            data: item,
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.matrixService.update(item.id, result).then(() => {
                    this.toastrService.info('Запись обновлена!');
                });
            }
        });
    }

    deleteItemMatrix(elem) {
        this.matrixService.delete(elem.id).then(() => {
            this.toastrService.error('Запись удалена!');
        });
    }
}


