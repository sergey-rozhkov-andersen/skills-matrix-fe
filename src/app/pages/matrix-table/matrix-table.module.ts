import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatrixDialogComponent} from '../../shared/matrix-dialog/matrix-dialog.component';
import {MatrixTableComponent} from './matrix-table.component';

@NgModule({
    declarations: [
        MatrixTableComponent,
        MatrixDialogComponent,
    ],
    imports: [
        MatTableModule,
        MatIconModule,
        MatDialogModule,
        MatInputModule,
        ReactiveFormsModule,
        MatButtonModule,
    ],
    exports: [
        MatrixTableComponent,
    ],
})
export class MatrixTableModule {
}
