import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {DomSanitizer} from '@angular/platform-browser';
import {ToastrService} from 'ngx-toastr';
import {CommentService} from '../../../core/services/comment.service';
import {UserService} from '../../../core/services/user.service';
import {IComment} from '../../../models/comment';
import {IUserDetails} from '../../../models/userdetails';
import {CommentDialogComponent} from '../../../shared/user-dialog/comment-dialog.component';
import {SearchComponent} from '../../search/search.component';

@Component({
    selector: 'app-index-user',
    templateUrl: './index-user.component.html',
    styleUrls: ['./index-user.component.scss'],
})

export class IndexUserComponent implements OnInit {
    users = [];
    displayedColumns: string[] = ['photo', 'fullName', 'location', 'roles', 'department', 'level', 'skype', 'phone', 'comment'];
    dataSource;
    tempFullName = '';
    tempLocation = '';
    tempLevel = '';
    @ViewChild(MatSort, {static: true}) sort: MatSort;

    constructor(
        private userService: UserService,
        private sanitizer: DomSanitizer,
        public dialog: MatDialog,
        public dialogSearch: MatDialog,
        public commentService: CommentService,
        private toastrService: ToastrService,
    ) {
    }

    public ngOnInit(): void {
        this.getUsers();
    }

    public getUsers(): void {
        this.userService.getAll().subscribe((el: IUserDetails[]) => {
            el.forEach((item: IUserDetails) => {
                this.users.push({
                    externalId: item.externalId,
                    photo: item.userPrivate.photo,
                    fullName: item.userPrivate.fullNameRu,
                    location: item.userPrivate.location.name,
                    roles: item.userMain.roles,
                    department: item.userMain.department,
                    level: item.userMain.level,
                    skype: item.userPrivate.skype,
                    phone: item.userPrivate.mobilePhone,
                });
            });
            this.dataSource = new MatTableDataSource(this.users);
            this.dataSource.sort = this.sort;
        });
    }

    public clearInputFullName() {
        this.tempFullName = '';
        this.searchLocation('');
    }

    public clearInputLocation() {
        this.tempLocation = '';
        this.searchFullName('');
    }

    public clearInputLevel() {
        this.tempLevel = '';
        this.searchLevel('');
    }

    public sortUser() {
        this.dataSource = new MatTableDataSource(this.users.filter(user => {
            return user.fullName.toLowerCase().includes(this.tempFullName.toLowerCase());
        }));
        this.dataSource = new MatTableDataSource(this.dataSource.data.filter(user => {
            return user.location.toLowerCase().includes(this.tempLocation.toLowerCase());
        }));
        this.dataSource = new MatTableDataSource(this.dataSource.data.filter(user => {
            return user.level.toLowerCase().includes(this.tempLevel.toLowerCase());
        }));
    }

    public searchFullName(e) {
        this.tempFullName = e;
        if (!this.tempLevel.trim() && !this.tempLocation.trim() && !this.tempFullName.trim()) {
            this.dataSource = new MatTableDataSource(this.users);
        } else {
            this.sortUser();
        }
        this.dataSource.sort = this.sort;
    }

    public searchLocation(e) {
        this.tempLocation = e;
        if (!this.tempLevel.trim() && !this.tempLocation.trim() && !this.tempFullName.trim()) {
            this.dataSource = new MatTableDataSource(this.users);
        } else {
            this.sortUser();
        }
        this.dataSource.sort = this.sort;
    }

    public searchLevel(e) {
        this.tempLevel = e;
        if (!this.tempLevel.trim() && !this.tempLocation.trim() && !this.tempFullName.trim()) {
            this.dataSource = new MatTableDataSource(this.users);
        } else {
            this.sortUser();
        }
        this.dataSource.sort = this.sort;
    }

    sanitizeSkype(url: string) {
        return this.sanitizer.bypassSecurityTrustUrl('skype:' + url + '?chat');
    }

    sanitizeTel(url: string) {
        return this.sanitizer.bypassSecurityTrustUrl('tel:' + url);
    }

    sanitizePortal(url: string) {
        return this.sanitizer.bypassSecurityTrustUrl('https://team.andersenlab.com/employee/' + url + '/personal-info');
    }

    public openDialog(id) {
        const dialogSub = this.commentService.getById(id).subscribe((res: IComment) => {
                const dialogRef = this.dialog.open(CommentDialogComponent, {
                    data: {id, reviewId: res?.reviewId, comment: res?.comment},
                });

                dialogRef.afterClosed().subscribe(result => {
                    if (result) {
                        if (!res) {
                            result = {id, reviewId: Date.now().toString(16), comment: result.comment} as IComment;
                            this.commentService.create(result).then(() => {
                                this.toastrService.success('Комментарий успешно добавлен!');
                            });
                        } else {
                            result = {id, reviewId: res.reviewId, comment: result.comment} as IComment;
                            this.commentService.update(id, result).then(() => {
                                this.toastrService.success('Комментарий успешно обновлен!');
                            });
                        }
                    }
                    dialogSub.unsubscribe();
                });
            },
            () => {
                this.toastrService.error('Не удалось получить комментарий пользователя!');
            });

    }

    public openSearchDialog() {
        this.dialogSearch.open(SearchComponent, {width: '1600px', height: '800px'});
    }
}
