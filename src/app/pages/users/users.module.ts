import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {CommentDialogComponent} from '../../shared/user-dialog/comment-dialog.component';
import {IndexUserComponent} from './index-user/index-user.component';

@NgModule({
    declarations: [
        IndexUserComponent,
        CommentDialogComponent,
    ],
    imports: [
        MatTableModule,
        MatIconModule,
        MatFormFieldModule,
        MatDialogModule,
        ReactiveFormsModule,
        MatInputModule,
        MatButtonModule,
        FormsModule,
        MatSortModule,
        MatCardModule,
    ],
    exports: [
        IndexUserComponent,
    ],
})
export class UsersModule {
}
