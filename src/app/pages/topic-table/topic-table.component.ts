import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {TopicService} from '../../core/services/topic.service';
import {ITopic} from '../../models/topic';
import {TopicDialogComponent} from '../../shared/topic-dialog/topic-dialog.component';

@Component({
    selector: 'app-topic-table',
    templateUrl: './topic-table.component.html',
    styleUrls: ['./topic-table.component.scss'],
})
export class TopicTableComponent implements OnInit {

    @ViewChild(TopicDialogComponent) modalEdit: TopicDialogComponent;
    topic: ITopic[];
    displayedColumns: string[] = ['position', 'title', 'description', 'number', 'level', 'comment', 'del'];

    constructor(public dialog: MatDialog,
                private topicService: TopicService,
                private toastrService: ToastrService,
    ) {
    }

    ngOnInit(): void {
        this.loadData();
    }

    public loadData() {
        this.topicService.getAll().subscribe((res) => {
            this.topic = res as ITopic[];
        }, (err) => {
            console.log(err);
        });
    }

    public openDialog() {
        const dialogRef = this.dialog.open(TopicDialogComponent, {
            data: {},
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                result.id = Date.now().toString(16);
                this.topicService.create(result).then(() => {
                    this.toastrService.success('Запись добавлена!');
                });
            }
        });
    }

    public editing(item): void {
        const dialogRef = this.dialog.open(TopicDialogComponent, {
            data: item,
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                result.id = item.id;
                this.topicService.update(result.id, result).then(() => {
                    this.toastrService.info('Запись обновлена!');
                });
            }
        });
    }

    public delete(elem) {
        this.topicService.delete(elem.id).then(() => {
            this.toastrService.error('Запись удалена!');
        });
    }
}
