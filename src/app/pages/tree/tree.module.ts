import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {MatBadgeModule} from '@angular/material/badge';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatTooltipModule} from '@angular/material/tooltip';
import {BrowserModule} from '@angular/platform-browser';
import {ButtonModule} from '@syncfusion/ej2-angular-buttons';
import {DiagramModule} from '@syncfusion/ej2-angular-diagrams';
import {UploaderModule} from '@syncfusion/ej2-angular-inputs';
import {ToolbarModule} from '@syncfusion/ej2-angular-navigations';
import {LoadDiagramDialogComponent} from '../../shared/load-diagram-dialog/load-diagram-dialog.component';
import {TreeCommentDialogComponent} from '../../shared/tree-comment-dialog/tree-comment-dialog.component';
import {UsersTreeDialog} from '../../shared/users-tree-dialog/users-tree-dialog';
import {TreeComponent} from './tree.component';

@NgModule({
    declarations: [
        TreeComponent,
        UsersTreeDialog,
        LoadDiagramDialogComponent,
        TreeCommentDialogComponent,
    ],
    imports: [
        ButtonModule,
        UploaderModule,
        BrowserModule,
        CommonModule,
        DiagramModule,
        MatCardModule,
        MatDialogModule,
        MatIconModule,
        MatButtonModule,
        ToolbarModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatDividerModule,
        MatBadgeModule,
        MatTooltipModule,
    ],
    exports: [
        TreeComponent,
    ],
    providers: [],
})
export class TreeModule {
}
