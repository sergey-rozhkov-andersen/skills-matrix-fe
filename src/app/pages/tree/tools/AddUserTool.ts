import {MatDialog} from '@angular/material/dialog';
import {cloneObject, CommandHandler, Connector, MoveTool, Node, randomId} from '@syncfusion/ej2-angular-diagrams';
import {ConnectorModel, Diagram, NodeModel} from '@syncfusion/ej2-diagrams';
import {UserService} from '../../../core/services/user.service';
import {IUserDetails} from '../../../models/userdetails';
import {UsersTreeDialog} from '../../../shared/users-tree-dialog/users-tree-dialog';

export class AddUserTool extends MoveTool {

    public diagram: Diagram = null;

    constructor(public command: CommandHandler, public dialog: MatDialog, public userService: UserService) {
        super(command);
    }

    static addConnector(source: NodeModel, target: NodeModel): ConnectorModel {
        const connector: ConnectorModel = {};
        connector.id = randomId();
        connector.sourceID = source.id;
        connector.targetID = target.id;
        return connector;
    }

    public mouseDown(): void {
        const childNodes: IUserDetails[] = [];
        const con = this.diagram.selectedItems.nodes[0] as Node;
        if (con) {
            childNodes.push((con.data as IUserDetails));
            const node = con.outEdges;
            console.log(node);
            node.forEach(item => {
                childNodes.push(
                    (this.diagram.getObject((this.diagram.getObject(item) as Connector).targetID) as Node).data as IUserDetails,
                );
            });
        }
        const dialogSub = this.userService.getAll().subscribe((res: IUserDetails[]) => {

            childNodes.forEach(item => {
                res = res.filter(e => e.externalId !== item.externalId);
            });
            const dialogRef = this.dialog.open(UsersTreeDialog, {
                data: res,
                width: '1600px',
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    const newObject: NodeModel | ConnectorModel = cloneObject(this.diagram.selectedItems.nodes[0]) as NodeModel;
                    newObject.data = result;
                    newObject.id = randomId();
                    if (this.diagram.selectedItems.nodes[0]) {
                        const connector: ConnectorModel = AddUserTool.addConnector(this.diagram.selectedItems.nodes[0], newObject);
                        const nd: Node = this.diagram.addNode(newObject);
                        this.diagram.addConnector(connector);
                        this.diagram.doLayout();
                        this.diagram.bringIntoView(nd.wrapper.bounds);
                    } else {
                        const nd: Node = this.diagram.addNode(newObject);
                        this.diagram.doLayout();
                        this.diagram.bringIntoView(nd.wrapper.bounds);
                    }
                }
                dialogSub.unsubscribe();
            });
        });
    }

}
