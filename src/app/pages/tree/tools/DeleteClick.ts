import {CommandHandler, Connector, MouseEventArgs, Node, ToolBase} from '@syncfusion/ej2-angular-diagrams';
import {ConnectorModel, Diagram, NodeModel} from '@syncfusion/ej2-diagrams';

export class DeleteClick extends ToolBase {
    public diagram: Diagram = null;
    protected inAction: boolean;

    constructor(command: CommandHandler, protectChange?: boolean) {
        super(command, protectChange);
    }

    public mouseDown(args: MouseEventArgs): void {
        const selectedObject: (NodeModel | ConnectorModel)[] = this.commandHandler.getSelectedObject();
        if (selectedObject[0]) {
            const node: NodeModel = selectedObject[0] as NodeModel;
            this.removeSubChild(node);
            this.diagram.doLayout();
        }
    }

    private removeSubChild(node: NodeModel): void {
        for (let i: number = (node as Node).outEdges.length - 1; i >= 0; i--) {
            const connector: Connector = this.diagram.getObject((node as Node).outEdges[i]) as Connector;
            const childNode: Node = this.diagram.getObject(connector.targetID) as Node;
            if (childNode.outEdges.length > 0) {
                this.removeSubChild(childNode);
            } else {
                this.diagram.remove(childNode);
            }
        }

        this.diagram.remove(node);
    }

}
