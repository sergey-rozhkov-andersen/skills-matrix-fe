import {HttpClientModule} from '@angular/common/http';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AngularFireModule} from '@angular/fire';
import {MatDialogModule} from '@angular/material/dialog';
import {ToastrModule} from 'ngx-toastr';
import {environment} from '../../../environments/environment';

import {TreeComponent} from './tree.component';

describe('TreeComponent', () => {
    let component: TreeComponent;
    let fixture: ComponentFixture<TreeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TreeComponent],
            imports: [
                MatDialogModule,
                HttpClientModule,
                AngularFireModule.initializeApp(environment.firebase),
                ToastrModule.forRoot({
                    timeOut: 3000,
                    positionClass: 'toast-custom',
                    preventDuplicates: true,
                    progressBar: true,
                }),
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TreeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
