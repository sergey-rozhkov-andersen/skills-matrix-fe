import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute} from '@angular/router';
import {
    Container,
    DiagramComponent,
    LayoutModel,
    Node,
    NodeConstraints,
    SelectorConstraints,
    SelectorModel,
    StackPanel,
    TextElement,
    TreeInfo,
    UserHandleModel,
} from '@syncfusion/ej2-angular-diagrams';
import {DataManager} from '@syncfusion/ej2-data';
import {ConnectorModel, DataBinding, Diagram, HierarchicalTree, ImageElement, NodeModel} from '@syncfusion/ej2-diagrams';
import {AsyncSettingsModel} from '@syncfusion/ej2-inputs';
import {ToastrService} from 'ngx-toastr';
import {UserTreeService} from '../../core/services/user-tree.service';
import {UserService} from '../../core/services/user.service';
import {IComment} from '../../models/comment';
import {IUserDetails} from '../../models/userdetails';
import {LoadDiagramDialogComponent} from '../../shared/load-diagram-dialog/load-diagram-dialog.component';
import {TreeCommentDialogComponent} from '../../shared/tree-comment-dialog/tree-comment-dialog.component';
import {AddUserTool} from './tools/AddUserTool';
import {DeleteClick} from './tools/DeleteClick';

Diagram.Inject(DataBinding, HierarchicalTree);

@Component({
    selector: 'app-tree',
    templateUrl: './tree.component.html',
    styleUrls: ['./tree.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class TreeComponent implements OnInit {
    @ViewChild('diagram')
    public diagram: DiagramComponent;
    public handles: UserHandleModel[] = [
        {
            name: 'add', side: 'Top', horizontalAlignment: 'Center', verticalAlignment: 'Center',
            pathData: 'M18,13 L13,13 L13,18 C13,18.55 12.55,19 12,19 C11.45,19 11,18.55 11,18 L11,13 L6,13'
                + 'C5.45,13 5,12.55 5,12 C5,11.45 5.45,11 6,11 L11,11 L11,6 C11,5.45 11.45,5 12,5 C12.55,5 '
                + '13,5.45 13,6 L13,11 L18,11 C18.55,11 19,11.45 19,12 C19,12.55 18.55,13 18,13 Z',
            visible: true,
            offset: 1,
            margin: {top: 5, bottom: 0, left: 0, right: 0},
        },
        {
            name: 'delete', side: 'Right', horizontalAlignment: 'Center', verticalAlignment: 'Center',
            pathData:
                'M 7.04 22.13 L 92.95 22.13 L 92.95 88.8 C 92.95 91.92 91.55 94.58 88.76' +
                '96.74 C 85.97 98.91 82.55 100 78.52 100 L 21.48 100 C 17.45 100 14.03 98.91 11.24 96.74 C 8.45 94.58 7.04' +
                '91.92 7.04 88.8 z M 32.22 0 L 67.78 0 L 75.17 5.47 L 100 5.47 L 100 16.67 L 0 16.67 L 0 5.47 L 24.83 5.47 z',
            visible: true,
            offset: 0,
            margin: {top: 5, bottom: 0, left: 0, right: 0},
        },
    ];
    public selectedItems: SelectorModel = {
        constraints: SelectorConstraints.UserHandle,
        userHandles: this.handles,
    };
    public getCustomTool = this.getTool.bind(this);
    public results: IUserDetails[] = [];
    public data;
    public layout: LayoutModel = {
        type: 'OrganizationalChart', margin: {top: 20},
        getLayoutInfo: (node: Node, tree: TreeInfo) => {
            if (!tree.hasSubTree) {
                tree.orientation = 'Vertical';
                tree.type = 'Right';
            }
        },
    };
    public asyncSettings: AsyncSettingsModel = {
        saveUrl: 'https://aspnetmvc.syncfusion.com/services/api/uploadbox/Save',
        removeUrl: 'https://aspnetmvc.syncfusion.com/services/api/uploadbox/Remove',
    };
    public treesData: IComment;
    id: string;

    constructor(public matDialog: MatDialog,
                private userService: UserService,
                private userTreeService: UserTreeService,
                private toastrService: ToastrService,
                private activateRoute: ActivatedRoute,
    ) {
    }

    ngOnInit() {
        this.data = {id: 'externalId', parentId: 'parentId', dataSource: new DataManager((this.results as IUserDetails[]))};
        this.activateRoute.params.subscribe(params => {
            this.id = params.id;
            if (this.id !== 'index') {
                this.getDiagramById(this.id);
            }
        });
    }

    public nodeDefaults(node: NodeModel): NodeModel {
        // tslint:disable-next-line:no-bitwise
        node.constraints = NodeConstraints.Default & ~NodeConstraints.Rotate & ~NodeConstraints.Resize;
        return node;
    }

    public connDefaults(connector: ConnectorModel): ConnectorModel {
        connector.targetDecorator.shape = 'None';
        connector.type = 'Orthogonal';
        connector.style.strokeColor = 'gray';
        return connector;
    }

    public setNodeTemplate(obj: NodeModel): Container {
        const content: StackPanel = new StackPanel();
        content.id = obj.id + '_outerstack';
        content.orientation = 'Horizontal';
        content.style.strokeColor = 'gray';
        content.padding = {left: 5, right: 10, top: 5, bottom: 5};

        const image: ImageElement = new ImageElement();
        image.width = 40;
        image.height = 50;
        image.style.strokeColor = 'none';
        image.source = (obj.data as IUserDetails).userPrivate?.photo;
        image.id = obj.id + '_pic';

        const innerStack: StackPanel = new StackPanel();
        innerStack.style.strokeColor = 'none';
        innerStack.margin = {left: 5, right: 0, top: 0, bottom: 0};
        innerStack.id = obj.id + '_innerstack';

        const text: TextElement = new TextElement();
        text.content = (obj.data as IUserDetails).userPrivate?.fullNameRu;
        text.style.color = 'black';
        text.style.bold = true;
        text.style.strokeColor = 'none';
        text.horizontalAlignment = 'Left';
        text.style.fill = 'none';
        text.id = obj.id + '_text1';

        const desigText: TextElement = new TextElement();
        desigText.margin = {left: 0, right: 0, top: 5, bottom: 0};
        if ((obj.data as IUserDetails).userMain?.level) {
            desigText.content = (obj.data as IUserDetails).userMain?.department + '/' + (obj.data as IUserDetails).userMain?.level;
        } else {
            desigText.content = (obj.data as IUserDetails).userMain?.department;
        }
        desigText.style.color = 'black';
        desigText.style.strokeColor = 'none';
        desigText.style.fontSize = 12;
        desigText.style.fill = 'none';
        desigText.horizontalAlignment = 'Left';
        desigText.style.textWrapping = 'Wrap';
        desigText.id = obj.id + '_desig';
        innerStack.children = [text, desigText];

        content.children = [image, innerStack];

        return content;
    }

    public saveLocal() {
        const data = this.diagram.saveDiagram();
        if (window.navigator.msSaveBlob) {
            const blob: Blob = new Blob([data], {type: 'data:text/json;charset=utf-8,'});
            window.navigator.msSaveOrOpenBlob(blob, 'Diagram.json');
        } else {
            const dataStr: string = 'data:text/json;charset=utf-8,' + encodeURIComponent(data);
            const a: HTMLAnchorElement = document.createElement('a');
            a.href = dataStr;
            a.download = 'Diagram.json';
            document.body.appendChild(a);
            a.click();
            a.remove();
        }
    }

    public loadLocal() {
        document.getElementsByClassName('e-file-select-wrap')[0].querySelector('button').click();
    }

    public onUploadSuccess(args): void {
        const file: Blob = ((args as { [key: string]: object }).file as { [key: string]: object }).rawFile as Blob;
        const reader: FileReader = new FileReader();
        reader.readAsText(file);
        reader.onloadend = this.loadDiagram.bind(this);
    }

    public loadDiagram(event: ProgressEvent): void {
        this.diagram.loadDiagram((event.target as FileReader).result.toString());
    }

    public loadDiagramFirebase() {
        this.userTreeService.getAll().subscribe((res) => {
            const dialogRef = this.matDialog.open(LoadDiagramDialogComponent, {
                data: res,
                width: '1600px',
            });

            dialogRef.afterClosed().subscribe((result: IComment) => {
                if (result) {
                    this.treesData = result;
                    this.diagram.loadDiagram(JSON.stringify(result.data));
                    this.diagram.doLayout();
                }
            });
        }, () => {
            this.toastrService.error('Не удалось получить список схем!');
        });
    }

    public saveDiagramFirebase() {
        const dialogRef = this.matDialog.open(TreeCommentDialogComponent, {
            data: this.treesData,
            width: '600px',
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                if (!this.treesData?.id) {
                    this.treesData = {
                        id: Date.now().toString(16),
                        name: result.name,
                        comment: result.comment,
                        reviewId: '',
                        data: this.diagram.saveDiagram(),
                    };
                    this.userTreeService.create(JSON.parse(
                        '{"data":' + this.treesData.data
                        + ',"id":"' + this.treesData.id
                        + '","comment":"' + this.treesData.comment
                        + '","name":"' + this.treesData.name + '"}'))
                        .then(() => {
                            this.toastrService.success('Схема успешно добавлена!');
                        });
                } else {
                    this.treesData = {
                        id: this.treesData.id,
                        name: result.name,
                        comment: result.comment,
                        reviewId: '',
                        data: this.diagram.saveDiagram(),
                    };
                    this.userTreeService.update(this.treesData.id, JSON.parse('{"data":' + this.treesData.data
                        + ',"id":"' + this.treesData.id
                        + '","comment":"' + this.treesData.comment + '", "name":"' + this.treesData.name + '"}'))
                        .then(() => {
                            this.toastrService.info('Схема успешно обновлена!');
                        });
                }
            }
        });
    }

    public addUserToDiargam() {
        const addUserTool: AddUserTool = new AddUserTool(this.diagram.commandHandler, this.matDialog, this.userService);
        addUserTool.diagram = this.diagram;
        addUserTool.mouseDown();
    }

    public clearDiagram() {
        this.diagram.clear();
        this.treesData = null;
    }

    public create(): void {
        this.diagram.fitToPage();
        this.diagram.dataBind();
    }

    public getTool(action: string) {
        if (action === 'add') {
            const addUserTool: AddUserTool = new AddUserTool(this.diagram.commandHandler, this.matDialog, this.userService);
            addUserTool.diagram = this.diagram;
            return addUserTool;
        } else if (action === 'delete') {
            let deleteTool: DeleteClick;
            deleteTool = new DeleteClick(this.diagram.commandHandler);
            deleteTool.diagram = this.diagram;
            return deleteTool;
        }
    }

    public getDiagramById(id) {
        this.userTreeService.getById(id).subscribe((res: IComment) => {
            this.treesData = res;
            this.diagram.loadDiagram(JSON.stringify(res.data));
            this.diagram.doLayout();
        });
    }
}
