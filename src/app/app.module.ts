import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {AngularFireModule} from '@angular/fire';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatToolbarModule} from '@angular/material/toolbar';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {environment} from '../environments/environment';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './component/header/header.component';
import {MenuContentComponent} from './component/menu-content/menu-content.component';
import {MenuHeaderComponent} from './component/menu-header/menu-header.component';
import {MenuNavComponent} from './component/menu-nav/menu-nav.component';
import {ErrorHandlerInterceptor} from './core/error-handler-interceptor';
import {RequestInterceptor} from './core/request-interceptor';
import {LoginComponent} from './pages/auth/login/login.component';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {MatrixTableModule} from './pages/matrix-table/matrix-table.module';
import {SearchModule} from './pages/search/search.module';
import {TopicTableModule} from './pages/topic-table/topic-table.module';
import {TreeModule} from './pages/tree/tree.module';
import {UsersModule} from './pages/users/users.module';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        HomePageComponent,
        MenuHeaderComponent,
        MenuNavComponent,
        MenuContentComponent,
        LoginComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatToolbarModule,
        MatButtonModule,
        MatListModule,
        MatInputModule,
        MatDialogModule,
        MatMenuModule,
        MatFormFieldModule,
        MatTableModule,
        ReactiveFormsModule,
        MatIconModule,
        FormsModule,
        TreeModule,
        MatrixTableModule,
        SearchModule,
        MatCardModule,
        TopicTableModule,
        UsersModule,
        SearchModule,
        AngularFireModule.initializeApp(environment.firebase),
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-custom',
            preventDuplicates: true,
            progressBar: true,
        }),
    ],
    providers: [{provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ErrorHandlerInterceptor, multi: true}],
    bootstrap: [AppComponent],
    exports: [],
})
export class AppModule {
}
