import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {IComment} from '../../models/comment';

@Component({
    selector: 'app-user-dialog',
    templateUrl: './comment-dialog.component.html',
    styleUrls: ['./comment-dialog.component.scss'],
})
export class CommentDialogComponent implements OnInit {

    form: FormGroup = new FormGroup({
        comment: new FormControl('', Validators.required),
    });

    constructor(public dialogRef: MatDialogRef<CommentDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: IComment) {
    }

    ngOnInit(): void {
        this.form.patchValue(this.data);
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onOkClick() {
        return this.form.value;
    }
}
