import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {IUserDetails} from '../../models/userdetails';

@Component({
    selector: 'app-users-tree-dialog',
    templateUrl: './users-tree-dialog.html',
    styleUrls: ['./users-tree-dialog.scss'],
})
// tslint:disable-next-line:component-class-suffix
export class UsersTreeDialog implements OnInit {

    userList: IUserDetails[] = [];
    title = 'Добавление пользователя';

    constructor(public dialogRef: MatDialogRef<UsersTreeDialog>,
                @Inject(MAT_DIALOG_DATA) public data: IUserDetails[]) {
    }

    ngOnInit(): void {
        this.userList = this.data;
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onOkClick(user) {
        return user;
    }
}
