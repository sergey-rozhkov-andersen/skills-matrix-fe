import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Matrix} from '../../models/matrix';

@Component({
    selector: 'app-topic-dialog',
    templateUrl: './topic-dialog.component.html',
    styleUrls: ['./topic-dialog.component.scss'],
})
export class TopicDialogComponent implements OnInit {

    form: FormGroup = new FormGroup({
        title: new FormControl('', Validators.required),
        description: new FormControl('', Validators.required),
        number: new FormControl('', Validators.required),
        level: new FormControl('', Validators.required),
        comment: new FormControl('', Validators.required),
    });

    title;

    constructor(public dialogRef: MatDialogRef<TopicDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: Matrix) {
    }

    ngOnInit(): void {

        if (this.data.id) {
            this.title = 'Редактирование';
            this.form.patchValue(this.data);
        } else {
            this.title = 'Добавление';
        }
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onOkClick() {
        return this.form.value;
    }
}
