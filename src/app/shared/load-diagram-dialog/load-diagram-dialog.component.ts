import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {UserTreeService} from '../../core/services/user-tree.service';

@Component({
    selector: 'app-user-dialog',
    templateUrl: './load-diagram-dialog.component.html',
    styleUrls: ['./load-diagram-dialog.component.scss'],
})
export class LoadDiagramDialogComponent implements OnInit {

    constructor(public dialogRef: MatDialogRef<LoadDiagramDialogComponent>,
                public userTreeService: UserTreeService,
                public router: Router,
                @Inject(MAT_DIALOG_DATA) public data) {
    }

    ngOnInit(): void {
        console.log('opana');
    }

    deleteClick(item) {
        this.userTreeService.delete(item.id);
        this.onNoClick();
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onOkClick(data) {
        this.router.navigate(['tree/', data.id]);
        return this.dialogRef.close();
    }
}
