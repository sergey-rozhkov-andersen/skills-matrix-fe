import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Matrix} from '../../models/matrix';

@Component({
    selector: 'app-dialog-add-edit-matrix',
    templateUrl: 'matrix-dialog.component.html',
    styleUrls: ['matrix-dialog.component.scss'],
})
export class MatrixDialogComponent implements OnInit {

    form: FormGroup = this.fb.group({
        interviewerId: ['', Validators.required],
        userId: ['', Validators.required],
        topics: ['', Validators.required],
        date: ['', Validators.required],
        results: ['', Validators.required],
    });
    title;

    constructor(public dialogRef: MatDialogRef<MatrixDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: Matrix,
                private fb: FormBuilder) {
    }

    ngOnInit() {
        if (this.data.id) {
            this.title = 'Редактирование';
            this.form.patchValue(this.data);
        } else {
            this.title = 'Добавление';
        }
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onOkClick() {
        return this.form.value;
    }

}
