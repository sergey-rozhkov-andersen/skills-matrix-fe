import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {IComment} from '../../models/comment';

@Component({
    selector: 'app-user-dialog',
    templateUrl: './tree-comment-dialog.component.html',
    styleUrls: ['./tree-comment-dialog.component.scss'],
})
export class TreeCommentDialogComponent implements OnInit {

    form: FormGroup = new FormGroup({
        name: new FormControl('', Validators.required),
        comment: new FormControl('', Validators.maxLength(200)),
    });

    constructor(public dialogRef: MatDialogRef<TreeCommentDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: IComment) {
    }

    ngOnInit(): void {
        if (this.data?.name) {
            this.form.patchValue(this.data);
        }
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onOkClick() {
        return this.form.value;
    }
}
