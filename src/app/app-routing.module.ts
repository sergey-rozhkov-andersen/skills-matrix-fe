import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './core/guard/auth.guard';
import {LoginComponent} from './pages/auth/login/login.component';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {MatrixTableComponent} from './pages/matrix-table/matrix-table.component';
import {TopicTableComponent} from './pages/topic-table/topic-table.component';
import {TreeComponent} from './pages/tree/tree.component';
import {IndexUserComponent} from './pages/users/index-user/index-user.component';

const routes: Routes = [
    {path: '', component: HomePageComponent},
    {
        path: 'users', component: IndexUserComponent,
        canActivate: [AuthGuard],
    },
    {
        path: 'matrix', component: MatrixTableComponent,
        canActivate: [AuthGuard],
    },
    {
        path: 'topic-table', component: TopicTableComponent,
        canActivate: [AuthGuard],
    },
    {
        path: 'tree/:id', component: TreeComponent,
        canActivate: [AuthGuard],
    },
    {path: 'login', component: LoginComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
