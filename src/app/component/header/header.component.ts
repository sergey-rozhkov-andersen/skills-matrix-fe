import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../core/services/auth.service';

@Component({
    selector: 'app-header',
    templateUrl: 'header.component.html',
    styleUrls: ['header.component.scss'],
})

export class HeaderComponent {
    email;

    constructor(public auth: AuthService,
                public router: Router) {
    }

    getName() {
        if (this.auth.isLogged()) {
            this.email = localStorage.getItem('userEmail');
        } else {
            this.email = 'Unauthorized';
        }
        return this.email;
    }
}
