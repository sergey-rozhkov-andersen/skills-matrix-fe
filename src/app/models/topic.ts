export interface ITopic {
    id?: number;
    title: string;
    description: string;
    number: number;
    level: string;
    comment: string;
}
