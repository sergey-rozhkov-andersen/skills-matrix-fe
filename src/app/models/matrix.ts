export interface Matrix {
    id: number;
    interviewerId: number;
    userId: number;
    topics: number[];
    date: Date;
    results: string;
}
