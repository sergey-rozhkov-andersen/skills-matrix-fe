export interface IComment {
    id: string;
    reviewId: string;
    name: string;
    comment: string;
    data;
}
