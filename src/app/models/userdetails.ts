export interface IUserDetails {
    parentId: string;
    externalId: string;
    projects: IProjects[];
    userDetail: IUserDetail;
    userMain: IUserMain;
    userPrivate: IUserPrivate;
}

export interface IUserDetail {
    id: string;
}

export interface IProjects {
    id: string;
    name: string;
    projectManager: { id: string, name: string };
    resourceManager: { id: string, name: string };
    teamQty: number;
}

export interface IUserMain {
    daysOff: object[];
    department: string;
    hrManager: { id: string, name: string };
    isWork: boolean;
    level: string;
    resourceManager: { id: string, name: string };
    roles: string[];
    startDate: string;
    technologyStackTags: [{ id: string, name: string }];
    type: string;
    vacations: object[];
}

export interface IUserPrivate {
    birthDate: string;
    email: string;
    fullNameEn: string;
    fullNameRu: string;
    location: { id: string, name: string };
    mobilePhone: string;
    photo: string;
    skype: string;
}

export interface IUser {
    name: string;
    photo: string;
    id: string;
}
