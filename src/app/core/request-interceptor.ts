import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (localStorage.getItem('access-token') != null) {
            return next.handle(req.clone({headers: req.headers.set('Authorization', 'Bearer ' + localStorage.getItem('access-token'))}));
        }
        return next.handle(req);
    }
}
