import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import {Router} from '@angular/router';
import {UserService} from './user.service';

@Injectable({
    providedIn: 'root',
})

export class AuthService {
    path = 'userLogin';

    constructor(public afAuth: AngularFireAuth,
                public userService: UserService,
                public router: Router,
                public firestore: AngularFirestore) {
    }

    get getUrl() {
        return this.firestore.collection(this.path);
    }

    login(email, password) {
        return this.afAuth.signInWithEmailAndPassword(email, password)
            .then((result) => {
                localStorage.setItem('userToken', result.user.uid);
                localStorage.setItem('userEmail', result.user.email);
                this.router.navigate(['users']);
            }).catch(() => {
                window.alert('Введён неправильный email или password');
            });
    }

    logout() {
        localStorage.removeItem('userToken');
        localStorage.removeItem('userEmail');
        this.router.navigate(['/']);
    }

    isLogged() {
        return !!localStorage.getItem('userToken');
    }

}

