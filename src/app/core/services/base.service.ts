import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable} from 'rxjs';

@Injectable()
export abstract class BaseService<T> {

    protected abstract path: string;

    protected constructor(
        public http: HttpClient,
        public firestore: AngularFirestore,
    ) {
    }

    get getUrl() {
        return this.firestore.collection(this.path);
    }

    public getAll(): Observable<any> {
        return this.getUrl.valueChanges();
    }

    public getById(id) {
        return this.getUrl.doc(id).valueChanges();
    }

    public delete(id: string) {
        return this.getUrl.doc(id).delete();
    }

    public create(data) {
        return this.getUrl.doc(data.id).set(data);
    }

    public update(id: string, data: T) {
        return this.getUrl.doc(id).update(data);
    }

}
