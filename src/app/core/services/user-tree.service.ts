import {Injectable} from '@angular/core';
import {BaseService} from './base.service';

@Injectable({
    providedIn: 'root',
})
export class UserTreeService extends BaseService<any> {
    path = 'userTree';
}
