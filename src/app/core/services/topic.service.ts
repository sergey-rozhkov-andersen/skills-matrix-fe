import {Injectable} from '@angular/core';
import {ITopic} from '../../models/topic';
import {BaseService} from './base.service';

@Injectable({
    providedIn: 'root',
})

export class TopicService extends BaseService<ITopic> {

    path = 'topic';
}
