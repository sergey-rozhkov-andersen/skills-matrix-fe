import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable()
export class TeamService {
    constructor(private http: HttpClient) {
    }

    getSearch(searchString) {
        return this.http.get('/api/filter/employeepreviews?startsWith=' + searchString);
    }

    getUser(id: string) {
        return this.http.get('/api/employee/' + id);
    }

    getMe() {
        return this.http.get('/api/account/userinfo');
    }
}
