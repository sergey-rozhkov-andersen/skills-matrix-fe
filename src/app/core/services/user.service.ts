import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {IUserDetails} from '../../models/userdetails';
import {BaseService} from './base.service';

@Injectable({
    providedIn: 'root',
})
export class UserService extends BaseService<IUserDetails> {

    path = 'user';

    constructor(public http: HttpClient,
                public firestore: AngularFirestore) {
        super(http, firestore);
    }

    public create(data: IUserDetails) {
        return this.firestore.collection(this.path).doc(data.externalId).set(data);
    }
}

