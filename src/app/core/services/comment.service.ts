import {Injectable} from '@angular/core';
import {IComment} from '../../models/comment';
import {BaseService} from './base.service';

@Injectable({
    providedIn: 'root',
})

export class CommentService extends BaseService<IComment> {

    path = 'comment';

}
