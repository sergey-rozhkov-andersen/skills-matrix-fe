import {Injectable} from '@angular/core';
import {Matrix} from '../../models/matrix';
import {BaseService} from './base.service';

@Injectable({
    providedIn: 'root',
})

export class MatrixService extends BaseService<Matrix> {

    path = 'matrix';
}

