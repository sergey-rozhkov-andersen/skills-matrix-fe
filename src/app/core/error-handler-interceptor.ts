import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import * as HttpStatus from 'http-status-codes';
import {ToastrService} from 'ngx-toastr';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {

    constructor(private toastrService: ToastrService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            tap(() => {
                },
                err => {
                    switch (err.status) {
                        case HttpStatus.UNAUTHORIZED :
                            this.toastrService.error('Требуеться авторизация');
                            console.log({error: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED)});
                            break;
                        case HttpStatus.FORBIDDEN :
                            this.toastrService.error('Недостаточно прав доступа');
                            console.log({error: HttpStatus.getStatusText(HttpStatus.FORBIDDEN)});
                            break;
                        case HttpStatus.INTERNAL_SERVER_ERROR:
                            this.toastrService.error('Ошибка на стороне сервера');
                            console.log({error: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR)});
                            break;
                        case HttpStatus.NOT_FOUND:
                            console.log({error: HttpStatus.getStatusText(HttpStatus.NOT_FOUND)});
                            break;
                    }
                },
            ),
        );
    }
}

