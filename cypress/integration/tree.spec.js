context('Users', () => {
    beforeEach(() => {
        cy.login();
        cy.visit(`/tree`);
    });


    it('AddUserToTree', () => {
        cy.getIdE2e('AddUserDiagram').click();
        cy.get('.user-header-text').first().invoke('text').then((item) => {
            cy.getIdE2e('AddUser').first().click();
            cy.wait(100).get('body').should('contain', item);
        });
    });

});
