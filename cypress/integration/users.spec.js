context('Users', () => {
    beforeEach(() => {
        cy.login();
        cy.visit(`/users`);
    });

    it('AddComment', () => {
        let comment = ' какой-то осмысленный комментарий';
        cy.getIdE2e('addComment').first().click();
        cy.getIdE2e('inputComment').invoke('val').then((item) => {
            cy.getIdE2e('inputComment').type(comment).should('have.value', item + comment);
            cy.getIdE2e('saveButton').click();
            cy.getIdE2e('addComment').first().click();
            cy.getIdE2e('inputComment').should('have.value', item + comment);
        });
    });

    it('SearchByFullName', () => {
        cy.wait(1500).getIdE2e('searchFullName').type('лан').should('have.value', 'лан');
        cy.get(':nth-child(1) > .cdk-column-fullName').should('contain', 'Ланских Евгений');
    });

    it('SearchByLocation', () => {
        cy.wait(1500).getIdE2e('searchLocation').type('Полоцк').should('have.value', 'Полоцк');
        cy.get(' :nth-child(1) > .cdk-column-location').should('contain', 'Полоцк');
    });

    it('SearchByLevel', () => {
        cy.wait(1500).getIdE2e('searchLevel').type('J1').should('have.value', 'J1');
        cy.get(' :nth-child(1) > .cdk-column-level').should('contain', 'J1');
    });

    it('SortByFullName', () => {
        cy.getIdE2e('sortByFullName').click();
        cy.get(':nth-child(1) > .cdk-column-fullName').should('contain', 'Акаченок Евгений');
        cy.getIdE2e('sortByFullName').click();
        cy.get(':nth-child(1) > .cdk-column-fullName').should('contain', 'Тарусов Максим');
    });

    it('SortByLocation', () => {
        cy.getIdE2e('sortByLocation').click();
        cy.get(':nth-child(1) > .cdk-column-location').should('contain', 'Витебск');
        cy.getIdE2e('sortByLocation').click();
        cy.get(':nth-child(1) > .cdk-column-location').should('contain', 'Черкассы');
    });

    it('SortByLevel', () => {
        cy.getIdE2e('sortByLevel').click();
        cy.get(':nth-child(1) > .cdk-column-level').should('contain', ' ');
        cy.getIdE2e('sortByLevel').click();
        cy.get(':nth-child(1) > .cdk-column-level').should('contain', 'S');
    });

});
