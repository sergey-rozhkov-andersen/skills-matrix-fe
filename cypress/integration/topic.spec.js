context('topic', () => {
    beforeEach(() => {
        cy.login();
    })

    it('topic', () => {
        cy.visit(`/topic-table`);
        cy.getIdE2e('createTopic').click();
        cy.readFile('cypress/fixtures/topic.json').then(res => {
            cy.getIdE2e('titleTopic').type(res.topicAddJSON.title);
            cy.getIdE2e('descriptionTopic').type(res.topicAddJSON.description);
            cy.getIdE2e('numberTopic').type(res.topicAddJSON.number);
            cy.getIdE2e('levelTopic').type(res.topicAddJSON.level);
            cy.getIdE2e('commentTopic').type(res.topicAddJSON.comment);
            cy.getIdE2e('btnTopicOk').click();
            cy.get('.cdk-column-level').contains(res.topicAddJSON.level);
        })
        cy.getIdE2e('editTopic').first().click();
        cy.readFile('cypress/fixtures/topic.json').then(res => {
            cy.getIdE2e('titleTopic').type(res.topicEditJSON.title);
            cy.getIdE2e('descriptionTopic').type(res.topicEditJSON.description);
            cy.getIdE2e('numberTopic').type(res.topicEditJSON.number);
            cy.getIdE2e('levelTopic').type(res.topicEditJSON.level);
            cy.getIdE2e('commentTopic').type(res.topicEditJSON.comment);
            cy.getIdE2e('btnTopicOk').click();
        })
        cy.readFile('cypress/fixtures/topic.json').then(res => {
            cy.wait(500).getIdE2e('deleteTopic').first().click().then(() => {
                cy.get('.cdk-column-level').contains(res.topicAddJSON.level);
            });
        })
    });
})