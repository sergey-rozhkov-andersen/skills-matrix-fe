context('Login', () => {
    beforeEach(() => {
    })

    it('Login', () => {
        cy.visit(`/login`);
        cy.getIdE2e('username').type('fake@email.com').should('have.value', 'fake@email.com');
        cy.getIdE2e('password').type('123456789').should('have.value', '123456789');
        cy.getIdE2e('singIn').click().url().should('match', /users/);
    })

})
