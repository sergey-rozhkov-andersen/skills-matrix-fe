context('search', () => {
    beforeEach(() => {
        cy.login();
    });

    it('Search', () => {
        cy.visit(`/search`);
        cy.getIdE2e('searchString').type('ник').should('have.value', 'ник');
        cy.getIdE2e('searchButton').click();
        cy.getIdE2e('addToStorage').first().click();
        cy.getIdE2e('userName').first().invoke('text').then(item => {
            cy.visit(`/users`);
            cy.get('.cdk-column-fullName').should('contain', item);
        });
    });
});
