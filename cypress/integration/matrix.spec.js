describe('Cypress Test', () => {
    beforeEach(() => {
        cy.login();
    });

    it('Create matrix', () => {
        cy.fixture('matrixCypress').then(data => {
            cy.visit(data.url);
            //Открытие модалочки
            cy.getIdE2e('addMatrix').click();
            //Заполнение инфой

            cy.getIdE2e('inputUserId').type(data.userId);
            cy.getIdE2e('inputInterviewId').type(data.idInterviewer);
            cy.getIdE2e('inputDate').type(data.date);
            cy.getIdE2e('inputTopics').type(data.topics);
            cy.getIdE2e('inputResults').type(data.results);
            //Клик по кнопке сейва
            cy.getIdE2e('saveMatrix').click();
            //Редактирование сабжей
            cy.get('.cdk-column-userId').should('contain', data.userId);
            cy.get('.cdk-column-date').should('contain', data.date);
            cy.getIdE2e('editMatrix').last().click();
            //Редактирование
            cy.getIdE2e('inputUserId').type(data.userId + 1);
            cy.getIdE2e('inputInterviewId').type(data.idInterviewer + 1);
            cy.getIdE2e('inputDate').type(data.date);
            cy.getIdE2e('inputTopics').type(data.topics + 1);
            cy.getIdE2e('inputResults').type(data.results + 999);
            //Клик по кнопке сейва
            cy.getIdE2e('saveMatrix').click();
            //Удаление элемента
            cy.getIdE2e('deleteMatrix').last().click();
        })
    });

});