// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
Cypress.Commands.add('getIdE2e', (e2e_id) => {
    return cy.get(`[e2e-id=${e2e_id}]`)
});
Cypress.Commands.add('login', () => {
    localStorage.setItem('userToken', 'gvZpmfufo2Q57JCShcMOjPkfn053');
    localStorage.setItem('userEmail', 'fake@email.com');
    localStorage.setItem('access-token', Cypress.env('team_token'));
});